const express = require('express')
const bodyParser = require('body-parser')

const app = express()

const sqlite = require('sqlite')
const dbConnection = sqlite.open('banco.sqlite', { Promise })

app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (req, res) => {
    res.send('Rodando...')
})

app.get('/admin', (req, res) => {
    res.render('admin/home')
})

app.get('/admin/vagas/editar/:id', async (req, res) => {
    const id = req.params.id

    const db = await dbConnection
    const categorias = await db.all('select * from categorias')

    const vaga = await db.get('select * from vagas where id = ' + id)

    res.render('admin/vaganova', {
        categorias,
        cat: vaga
    })
});

app.get('/admin/vagas/nova', async (req, res) => {
    const db = await dbConnection
    const categorias = await db.all('select * from categorias')

    const cat = {
        id: 0,
        titulo: '',
        descricao: '',
        categoria: 0
    }

    res.render('admin/vaganova', {
        categorias,
        cat
    })
})

app.post('/admin/vagas/nova', async (req, res) => {
    const db = await dbConnection

    const idVaga = req.body.id
    const tituloVaga = req.body.titulo
    const descricaoVaga = req.body.descricao
    const idCategoria = req.body.categoria

    if (idVaga > 0) {
        await db.run('update vagas set categoria = "' + idCategoria + '", titulo="' + tituloVaga + '", descricao="' + descricaoVaga + '" where id = ' + idVaga)
    } else {
        await db.run('insert into vagas (categoria, titulo, descricao) values (' + idCategoria + ', "' + tituloVaga + '", "' + descricaoVaga + '")')
    }

    res.redirect('/admin/vagas')
})

app.get('/admin/vagas', async (req, res) => {
    const db = await dbConnection
    const vagas = await db.all('select * from vagas')

    res.render('admin/vagas', {
        vagas
    })
})

app.get('/admin/vagas/delete/:id', async (req, res) => {
    const id = req.params.id

    const db = await dbConnection
    await db.run('delete from vagas where id = ' + id)

    res.redirect('/admin/vagas')
})

app.get('/home', async (req, res) => {
    const db = await dbConnection
    const categorias = await db.all('select * from categorias')
    const vagas = await db.all('select * from vagas')

    const colCategorias = categorias.map(cat => {
        return {
            ...cat,
            vagas: vagas.filter(v => v.categoria == cat.id)
        }
    })

    res.render('home', {
        usuario: 'anderson',
        colCategorias
    })
})

app.get('/admin/categorias/editar/:id', async (req, res) => {
    const id = req.params.id

    const db = await dbConnection
    const categoria = await db.get('select * from categorias where id = ' + id)

    res.render('admin/categorianova', {
        cat: categoria
    })
});

app.get('/admin/categorias/nova', async (req, res) => {
    const db = await dbConnection

    const cat = {
        id: 0,
        titulo: '',
        descricao: '',
        categoria: 0
    }

    res.render('admin/categorianova', {
        cat
    })
})

app.post('/admin/categorias/nova', async (req, res) => {
    const db = await dbConnection

    const idCategoria = req.body.id
    const tituloCategoria = req.body.titulo

    if (idCategoria > 0) {
        await db.run('update categorias set titulo="' + tituloCategoria + '" where id = ' + idCategoria)
    } else {
        await db.run('insert into categorias (titulo) values ("' + tituloCategoria + '")')
    }

    res.redirect('/admin/categorias')
})

app.get('/admin/categorias', async (req, res) => {
    const db = await dbConnection
    const categorias = await db.all('select * from categorias')

    res.render('admin/categorias', {
        categorias
    })
})

app.get('/admin/categorias/delete/:id', async (req, res) => {
    const id = req.params.id

    const db = await dbConnection
    await db.run('delete from categorias where id = ' + id)

    res.redirect('/admin/categorias')
})

app.get('/vaga/:id', async (req, res) => {
    const id = req.params.id

    const db = await dbConnection
    const vaga = await db.get('select * from vagas where id = ' + id)

    res.render('vaga', {
        vaga
    })
})

const init = async () => {
    const db = await dbConnection

    await db.run('create table if not exists categorias (id INTEGER PRIMARY KEY, titulo TEXT)')
    // const novaCategoria = "Marketing team"
    // await db.run('insert into categorias (titulo) values ("' + novaCategoria + '")')

    await db.run('create table if not exists vagas (id INTEGER PRIMARY KEY, categoria INTEGER, titulo TEXT, descricao TEXT)')
    // const tituloVaga = "terceira nova vaga"
    // const descricaoVaga = "terceira descricao desta vaga"
    // const idCategoria = 2
    // await db.run('insert into vagas (categoria, titulo, descricao) values (' + idCategoria + ', "' + tituloVaga + '", "' + descricaoVaga + '")')
}

init();

app.listen(3003, (err) => {
    if (!err) {
        console.log('Running in port 3003...')
    }
})